#include <gst/gst.h>
#include <gst/app/gstappsrc.h>

#ifdef __APPLE__
#include <TargetConditionals.h>
#endif

#include <stdio.h>
#include <glib/gprintf.h>

#include <sys/socket.h>
#include <arpa/inet.h>  // inet_addr
#include <unistd.h>     // close

#define BUFFER_SZ 8196
#define PORT      5004
#define MODE_RTP     1


struct BusData {
  int verbose;
  int sock;
  struct sockaddr_in serverAddr;
  GMainLoop * loop;
  GstElement * appsrc;
  int feed_need;
};

int init_socket(struct BusData * data)
{
  int sockfd = -1;

  sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sockfd == -1) {
    g_error("Could not create socket");
    return -1;
  }
  memset(&data->serverAddr, 0, sizeof(data->serverAddr));
  data->serverAddr.sin_family      = AF_INET;
  data->serverAddr.sin_port        = htons( PORT );
  data->serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  //bind socket to port
  if( bind(sockfd, (struct sockaddr*) &data->serverAddr, sizeof(data->serverAddr) ) == -1) {
    g_error("Could not bind socket");
    return -1;
  }
  return sockfd;
}

static void
start_feed (GstElement * pipeline, guint size, void * ptr)
{
  struct BusData * data = (struct BusData *) ptr;
  data->feed_need = 1;
}

static void
stop_feed (GstElement * pipeline, void * ptr)
{
  struct BusData * data = (struct BusData *) ptr;
  data->feed_need = 0;
}

void * recv_function(void * ptr)
{
  struct BusData * data = (struct BusData *) ptr;

  ssize_t received = 0;
  char buffer[BUFFER_SZ];
  memset(buffer, 0, BUFFER_SZ);

  GstFlowReturn ret;

  for (;;) {

    if ( ! data->feed_need ) {
      g_printf("No need\n");
      sleep(1);
      continue;
    }

    memset(buffer, 0, BUFFER_SZ);
    received = recv(data->sock, buffer, BUFFER_SZ, 0);

    // g_printf("recv:%d\n", received);

    if ( received < 0 ) {
      g_error("Received %d, exiting ...", received);
      break;
    }

    GstBuffer * buffer = gst_buffer_new_allocate(NULL, received, NULL);
    if ( ! buffer) {
      g_error("Failed to alloc buffer %d bytes", received);
      continue;
    }
    GstMapFlags flags = (GstMapFlags) (GST_MAP_READ | GST_MAP_WRITE);
    GstMapInfo info;
    if (gst_buffer_map(buffer, &info, flags) == 0) {
      g_error("Failed to map buffer");
      gst_buffer_unref(buffer);
      continue;
    }

    if ((int) info.size != received) {
      g_error("map info sz %d != %d sz", info.size, received);
      gst_buffer_unref(buffer);
      continue;
    }

    memcpy(info.data, buffer, received);

    gst_buffer_unmap (buffer, &info);

#if 0
    /* Push the buffer into the appsrc */
    g_signal_emit_by_name (data->appsrc, "push-buffer", buffer, &ret);
    if (ret != GST_FLOW_OK) {
      g_error("push err");
    } else {
      // g_printf("pushed:%d\n", received);
    }
    gst_buffer_unref (buffer);
#else
    if (gst_app_src_push_buffer(GST_APP_SRC(data->appsrc), buffer) != GST_FLOW_OK) {
      // gst_buffer_unref(buffer);
      g_error("push err");
    } else {
      // g_printf("pushed:%d\n", received);
    }
#endif
  } // for

  int retVal = 0;
  pthread_exit(&retVal);
} // recv_function()

int
tutorial_main (int argc, char *argv[])
{
  GstElement * pipeline;
  GstBus     * bus;
  GstMessage * msg;

  pthread_t recv_thread;
  int recv_ret;

  struct BusData busData;
  busData.verbose =  1;
  busData.sock    = init_socket(&busData);
  busData.feed_need = 0;

  printf("sock=%d\n", busData.sock);

  if ( busData.sock < 0 ) {
    g_error("Socket error");
    return -1;
  }

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  /* Build the pipeline */
#ifdef MODE_RTP
  const gchar * pipe_str =
        "appsrc name=\"mysrc\" format=time do-timestamp=true is-live=true stream-type=stream ! "
        "capsfilter caps=\"application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96\" ! "
        "rtpjitterbuffer latency=200 ! "
        "rtph264depay ! "
        "queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! "
        "h264parse ! "
        "avdec_h264 ! "
        "videoconvert ! "
        "autovideosink"
        ;
#else
  const gchar * pipe_str =
        "appsrc name=\"mysrc\" format=time do-timestamp=true is-live=true stream-type=stream ! "
        "queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! "
        "tsparse ! "
        "tsdemux ! "
        "queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! "
        "h264parse ! "
        "avdec_h264 ! "
        "videoconvert ! "
        "autovideosink"
        ;
#endif

  if (busData.verbose)
    g_printf("pipeline: %s\n", pipe_str);

  pipeline = gst_parse_launch (pipe_str,NULL);


  busData.appsrc = gst_bin_get_by_name(GST_BIN(pipeline), "mysrc");
  if ( ! busData.appsrc) {
    g_error("Gst Error: Failed to get appsrc named mysrc");
    return -1;
  }

  g_signal_connect (busData.appsrc, "need-data",   G_CALLBACK (start_feed), &busData);
  g_signal_connect (busData.appsrc, "enough-data", G_CALLBACK (stop_feed),  &busData);

  /* Start playing */
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  recv_ret = pthread_create( &recv_thread, NULL, recv_function, (void*) &busData);

  /* Wait until error or EOS */
  bus = gst_element_get_bus (pipeline);
  msg =
      gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE,
      GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

  /* See next tutorial for proper error message handling/parsing */
  if (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR) {
    g_error ("An error occurred! Re-run with the GST_DEBUG=*:WARN environment "
        "variable set for more details.");
  }

  pthread_join( recv_thread, NULL);
  printf("Recv Thread returns: %d\n", recv_ret);

  if (busData.sock >= 0)
    close(busData.sock);

  /* Free resources */
  gst_object_unref(busData.appsrc);
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  return 0;
}

int
main (int argc, char *argv[])
{
#if defined(__APPLE__) && TARGET_OS_MAC && !TARGET_OS_IPHONE
  return gst_macos_main ((GstMainFunc) tutorial_main, argc, argv, NULL);
#else
  return tutorial_main (argc, argv);
#endif
}
