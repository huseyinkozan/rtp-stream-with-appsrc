#include <gst/gst.h>

#ifdef __APPLE__
#include <TargetConditionals.h>
#endif

#include <stdio.h>
#include <glib/gprintf.h>

#include <sys/socket.h>
#include <arpa/inet.h>  // inet_addr
#include <unistd.h>     // close

#define PORT      5004
#define MODE_RTP     1


struct BusData {
  int verbose;
  int sock;
  struct sockaddr_in serverAddr;
};

GstFlowReturn appsink_new_sample(GstElement *sink, struct BusData *data)
{
  GstSample * sample = NULL;
  GstBuffer * buffer = NULL;
  GstMapInfo  mapInfo;
  ssize_t     sent = 0;

  if (data->sock < 0) {
    if (data->verbose)
      g_error("sock is null");
    return GST_FLOW_ERROR;
  }

  g_signal_emit_by_name(sink, "pull-sample", &sample);
  if ( ! sample) {
    if (data->verbose)
      g_error("Error: no sample at new_sample()");
    return GST_FLOW_ERROR;
  }
  if ((buffer = gst_sample_get_buffer(sample)) == NULL) {
    if (data->verbose)
      g_error("Error: buffer is NULL at new_sample()");
    return GST_FLOW_ERROR;
  }
  if (gst_buffer_get_size(buffer) == 0) {
    if (data->verbose)
      g_error("Error: buffer size is zero at new_sample()");
    return GST_FLOW_ERROR;
  }
  if ( ! gst_buffer_map(buffer, &mapInfo, GST_MAP_READ)) {
    if (data->verbose)
      g_error("Error: map failed at new_sample()");
    gst_sample_unref(sample);
    return GST_FLOW_ERROR;
  }

  sent = sendto(data->sock, mapInfo.data, mapInfo.size, 0,
    (struct sockaddr *) &data->serverAddr, sizeof(data->serverAddr));

  // g_printf("sent = %d, size = %d\n", sent, mapInfo.size);

  if (sent < 0) {
    if (data->verbose)
      g_error("sock send failed, sent=%d, Error:(%d) %s", sent, errno, strerror(errno));
    gst_buffer_unmap(buffer, &mapInfo);
    gst_sample_unref(sample);
    return GST_FLOW_ERROR;
  }

  gst_buffer_unmap(buffer, &mapInfo);
  gst_sample_unref(sample);
  return GST_FLOW_OK;
}

int init_socket(struct BusData * data)
{
  int sockfd = -1;

  sockfd = socket(AF_INET , SOCK_DGRAM , 0); // UDP
  if (sockfd == -1) {
    g_error("Could not create socket");
    return -1;
  }
  memset(&data->serverAddr, 0, sizeof(data->serverAddr));
  data->serverAddr.sin_family      = AF_INET;
  data->serverAddr.sin_port        = htons( PORT );
  data->serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  return sockfd;
}

int
tutorial_main (int argc, char *argv[])
{
  GstElement * pipeline;
  GstBus     * bus;
  GstMessage * msg;
  GstElement * appsink;

  struct BusData busData;
  busData.verbose =  1;
  busData.sock    = init_socket(&busData);

  printf("sock=%d\n", busData.sock);

  if ( busData.sock < 0 ) {
    g_error("Socket error");
    return -1;
  }

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  /* Build the pipeline */
#ifdef MODE_RTP
  const gchar * pipe_str =
        "v4l2src device=/dev/video0 do-timestamp=true typefind=true ! "
        "videoconvert ! "
        "videoflip video-direction=0 ! "
        "video/x-raw,width=800,height=600 ! "
        "x264enc intra-refresh=false bitrate=700 ! "
        "h264parse config-interval=-1 ! "
        "rtph264pay config-interval=-1 mtu=277 ! "
        "queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! "
        "appsink name=\"mysink\" emit-signals=true"
        ;
#else
  const gchar * pipe_str =
        "v4l2src device=/dev/video0 do-timestamp=true typefind=true ! "
        "videoconvert ! "
        "videoflip video-direction=0 ! "
        "video/x-raw,width=800,height=600 ! "
        "x264enc intra-refresh=false bitrate=700 ! "
        "h264parse config-interval=-1 ! "
        "mpegtsmux ! "
        "queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! "
        "appsink name=\"mysink\" emit-signals=true"
        ;
#endif

  if (busData.verbose)
    g_printf("pipeline: %s\n", pipe_str);

  pipeline = gst_parse_launch (pipe_str,NULL);


  appsink = gst_bin_get_by_name(GST_BIN(pipeline), "mysink");
  if ( ! appsink) {
    g_error("Gst Error: Failed to get appsink named mysink");
    return -1;
  }
  if ( ! g_signal_connect(G_OBJECT(appsink), "new-sample", G_CALLBACK(appsink_new_sample), &busData)) {
    g_error("Failed to connect appsink callback");
    return -1;
  }

  /* Start playing */
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  /* Wait until error or EOS */
  bus = gst_element_get_bus (pipeline);
  msg =
      gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE,
      GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

  /* See next tutorial for proper error message handling/parsing */
  if (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR) {
    g_error ("An error occurred! Re-run with the GST_DEBUG=*:WARN environment "
        "variable set for more details.");
  }

  if (busData.sock >= 0)
    close(busData.sock);

  /* Free resources */
  gst_message_unref (msg);
  gst_object_unref (bus);
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);
  return 0;
}

int
main (int argc, char *argv[])
{
#if defined(__APPLE__) && TARGET_OS_MAC && !TARGET_OS_IPHONE
  return gst_macos_main ((GstMainFunc) tutorial_main, argc, argv, NULL);
#else
  return tutorial_main (argc, argv);
#endif
}
