# Gstreamer RTP over TCP

## Sender

appsink:
```bash
gcc basic-tutorial-sender.c -o basic-tutorial-sender `pkg-config --cflags --libs gstreamer-1.0` && ./basic-tutorial-sender
```

TS with udpsink:
```bash
gst-launch-1.0 -v v4l2src device=/dev/video0 do-timestamp=true typefind=true ! videoconvert ! videoflip video-direction=0 ! video/x-raw,width=800,height=600 ! x264enc intra-refresh=false bitrate=700 ! h264parse config-interval=-1 ! mpegtsmux ! queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! udpsink host=127.0.0.1 port=5004
```

RTP with udpsink
```bash
gst-launch-1.0 -v v4l2src device=/dev/video0 do-timestamp=true typefind=true ! videoconvert ! videoflip video-direction=0 ! video/x-raw,width=800,height=600 ! x264enc intra-refresh=false bitrate=700 ! h264parse config-interval=-1 ! rtph264pay config-interval=-1 mtu=277 ! queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! udpsink host=127.0.0.1 port=5004
```

## Receiver

appsrc:
```bash
gcc basic-tutorial-receiver.c -o basic-tutorial-receiver `pkg-config --cflags --libs gstreamer-1.0` `pkg-config --cflags --libs gstreamer-app-1.0` && GST_DEBUG="*:2" ./basic-tutorial-receiver
```

TS with udpsrc:
```bash
gst-launch-1.0 -v udpsrc address=0.0.0.0 port=5004 do-timestamp=true typefind=true ! queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! tsparse ! tsdemux ! queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! h264parse ! avdec_h264 ! videoconvert ! ximagesink
```

RTP with udpsrc:
```bash
gst-launch-1.0 -v udpsrc address=0.0.0.0 port=5004 ! 'application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96' ! rtpjitterbuffer latency=200 ! rtph264depay ! queue max-size-buffers=0 max-size-bytes=0 max-size-time=10000 ! h264parse ! avdec_h264 ! videoconvert ! ximagesink
```
